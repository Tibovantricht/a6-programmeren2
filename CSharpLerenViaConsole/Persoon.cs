﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Blog
{
    class Persoon
    {
        //public string voornaam;
        //public string familienaam;
        //private int leeftijd;

        //public int Leeftijd
        //{
        //    get { return leeftijd; }
        //    set
        //    {
        //        if (value >= 0 && value <= 120)
        //        {
        //            leeftijd = value;
        //        }
        //        else
        //        {
        //            Console.WriteLine($"{value} is geen geldige leeftijd! Leeftijd tussen 0 en 120");
        //        }
        //    }
        //}

        //public string ShowInfo()
        //{
        //    return $"Voornaam: {voornaam}\nFamilienaam: {familienaam}\n leeftijd: {leeftijd}";
        //}
        private string voornaam;

        //properties maken we public = pascalnotatie
        public string Voornaam
        {
            get { return voornaam; }
            set { voornaam = value; }
        }

        private string familienaam;

        public string Familienaam
        {
            get { return familienaam; }
            set { familienaam = value; }
        }

        private string geslacht;

        public string Geslacht
        {
            get { return geslacht; }
            set { geslacht = value; }
        }

        private string leeftijd;

        public string Leeftijd
        {
            get { return leeftijd; }
            set { leeftijd = value; }
        }

        private string adres;

        public string Adres
        {
            get { return adres; }
            set { adres = value; }
        }

        public string SerializeObjectToCsv(List<Persoon> list, string separator)
        {
            string fileName = @"Data/Persoon.csv";
            string message;
            try
            {
                TextWriter writer = new StreamWriter(fileName);
                foreach (Persoon item in list)
                {
                    // One of the most versatile and useful additions to the C# language in version 6
                    // is the null conditional operator ?.           
                    writer.WriteLine("{0}{5}{1}{5}{2}{5}{3}{5}{4}",
                        item?.Voornaam,
                        item?.Familienaam,
                        item?.Geslacht,
                        item?.Leeftijd,
                        item?.adres,
                        separator);
                }
                writer.Close();
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }

            return message;
        }
    }
}
